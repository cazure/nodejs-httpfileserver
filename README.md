## nodejs-httpfileserver

http文件服务器。直接在根目录运行，适合临时使用http传输文件。

使用 Express 是一个简洁而灵活的 node.js Web 应用框架。

| 功能             | 说明                         | 路径             |
| ---------------- | ---------------------------- | ---------------- |
| 支持文件目录浏览 | 显示日期时间大小             | 根目录所有文件夹 |
| 支持文件下载     | 原始数据, 非Chunked编码传输  | 根目录所有文件   |
| 支持文件上传     | multipart/form-data 方式传输 | /upload/form     |
| 日志输出         |                              |                  |





wget 测试下载文件

```bash
wget http://192.168.1.98:18080/httpfileserver.js httpfileserver.js
```



curl 测试post上传数据

```bash
curl  http://192.168.1.98:18080/upload/form -v -F "file=@./filename"
```

